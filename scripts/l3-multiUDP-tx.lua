local mg     = require "moongen"
local memory = require "memory"
local device = require "device"
local ts     = require "timestamping"
local filter = require "filter"
local hist   = require "histogram"
local stats  = require "stats"
local timer  = require "timer"
local arp    = require "proto.arp"
local log    = require "log"

local DEF_ETH_DST       = "0C:C4:7A:EA:06:B5"
local DEF_ETH_SRC       = "0C:C4:7A:EA:06:B4"
local DEF_IP_DST        = "10.0.33.11"
local DEF_IP_SRC        = "10.0.33.12"

function configure(parser)
  parser:description("Generates UDP traffic ")
  parser:argument("dev1", "Device 1 to transmit from."):convert(tonumber)
  parser:argument("dev2", "Device 2 to transmit from."):convert(tonumber)
  parser:option("-r --rate", "Transmit rate in Mbit/s."):default(10000):convert(tonumber)
  parser:option("-s --size", "Packet size."):default(100):convert(tonumber)
  parser:option("-S --macsrc", "Mac source."):default(DEF_ETH_SRC)
  parser:option("-D --macdst", "MAC destination."):default(DEF_ETH_DST)
  parser:option("--ipsrc", "MAC destination."):default(DEF_IP_SRC)
  parser:option("--ipdst", "MAC destination."):default(DEF_IP_DST)
  parser:option("-w --wait", "Max wait time of receive packet."):default(15):convert(tonumber)
end

function master(args)
    print('macsrc: ',args.macsrc)
    local dev1 = device.config({port = args.dev1, txQueues = 6, rxQueues = 3})
    local dev2 = device.config({port = args.dev2, txQueues = 6, rxQueues = 3})
    device.waitForLinks()
    if args.rate > 0 then
      dev1:getTxQueue(0):setRate(args.rate - (args.size + 4) * 8 / 1000)
    end
    mg.startTask("loadSlave", dev1:getTxQueue(0), args)
    mg.waitForTasks()
end

local function fillUdpPacket(buf, args)
   buf:getUdpPacket():fill{
     ethSrc = args.macsrc,
     ethDst = args.macdst,
     ip4Src = args.ipsrc,
     ip4Dst = args.ipdst,
     udpSrc = args.portsrc,
     udpDst = args.portdst,
     pktLength = len
   }
end

function loadSlave(txq,args)
    local txbufs = memory.bufArray(1024)
    local txQueue = txq
    local txCtr = stats:newDevTxCounter(txQueue, "plain")
    local mem = memory.createMemPool(
                    function(buf) 
                        fillUdpPacket(buf, args) 
                     end)
    local txbufs = mem:bufArray()
    mg.sleepMillis(2000)
    while mg.running() do
        txbufs:alloc(args.size)
        txbufs:offloadUdpChecksums()
        txQueue:send(txbufs)
        txCtr:update()
    end
    print("\n//=========TX COUNTER STATS========//")
    txCtr:finalize()
end

function tdump ( tab, iter, indent )
   --print('//=== TDUMP ===//')
   if iter == nil then iter = 10 end
   if indent == nil then indent = "" end
   indent = indent.."   "
   if type (tab) ~= "table" then
      print ("invoke with a table, you sent me a: ",type (tab) )
      return   
   end
   for k,v in pairs (tab) do
      if type (v) == "table" then
         print (indent.." "..k.." <TABLE> ",v)
         if iter > 1 then
            tdump (v, iter-1, indent)
         end
      elseif type (v) == "function" then
         print (indent.." "..k.." <FUNCTION> ",v) 
      elseif type (v) == "boolean" then
         print (indent.." "..k.." <BOOLEAN> ",v) 
      else
         print (indent.." "..k.." ".." = ".." "..v) 
      end
   end
end

function printStats (devStats)
    print("ipacktes: " .. tostring(devStats.ipackets))
    print("opacktes: " .. tostring(devStats.opackets))
    print("ibytes: " .. tostring(devStats.ibytes))
    print("obytes: " .. tostring(devStats.obytes))
    print("imissed: " .. tostring(devStats.imissed))
    print("ierrors: " .. tostring(devStats.ierrors))
    print("oerrors: " .. tostring(devStats.oerrors))
    print("rx_nombuf: " .. tostring(devStats.rx_nombuf))
    print("q_ipacktes[0]: " .. tostring(devStats.q_ipackets[0]))
    print("q_ipacktes[1]: " .. tostring(devStats.q_ipackets[1]))
    print("q_ipacktes[2]: " .. tostring(devStats.q_ipackets[2]))
    print("q_ipacktes[3]: " .. tostring(devStats.q_ipackets[3]))
end

