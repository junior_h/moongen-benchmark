-- 23:53:10.714430 ARP, Ethernet (len 6), IPv4 (len 4), Request who-has 10.0.31.11 (Broadcast) tell 10.0.31.30, length 46
--         0x0000:  ffff ffff ffff 0cc4 7aea 06b4 0806 0001  ........z.......
--         0x0010:  0800 0604 0001 0cc4 7aea 06b4 0a00 1f1e  ........z.......
--         0x0020:  ffff ffff ffff 0a00 1f0b 0000 0000 0000  ................
--         0x0030:  0000 0000 0000 0000 0000 0000            ............
-- 23:53:10.714444 ARP, Ethernet (len 6), IPv4 (len 4), Reply 10.0.31.11 is-at 0c:c4:7a:bc:41:8c (oui Unknown), length 28
--         0x0000:  0cc4 7aea 06b4 0cc4 7abc 418c 0806 0001  ..z.....z.A.....
--         0x0010:  0800 0604 0002 0cc4 7abc 418c 0a00 1f0b  ........z.A.....
--         0x0020:  0cc4 7aea 06b4 0a00 1f1e                 ..z.......

local mg     = require "moongen"
local memory = require "memory"
local device = require "device"
local ts     = require "timestamping"
local filter = require "filter"
local hist   = require "histogram"
local stats  = require "stats"
local timer  = require "timer"
local arp    = require "proto.arp"
local log    = require "log"

--local DEF_ETH_SRC	= "12:34:56:78:90:ab"
local DEF_SRC_IP	= '10.0.31.30'
local DEF_DST_IP	= '10.0.31.11'
local DST_MAC		= nil

function configure(parser)
  parser:description("Generates UDP traffic ")
  parser:argument("dev1", "Device 1 to transmit from."):convert(tonumber)
  --parser:argument("dev2", "Device 2 to transmit from."):convert(tonumber)
  --parser:option("--macsrc", "MAC source."):default(DEF_ETH_SRC)
  parser:option("--ipsrc", "IP source."):default(DEF_SRC_IP)
  parser:option("--ipdst", "IP source."):default(DEF_DST_IP)
end

function master(args)
  local dev1 = device.config({port = args.dev1, rxQueues = 3, txQueues = 3})
  --local dev2 = device.config({port = args.dev2, rxQueues = 3, txQueues = 3})
  device.waitForLinks()
  mg.startSharedTask("loadSlave", dev1:getTxQueue(0), args)
  --mg.startSharedTask("loadSlave", dev2:getTxQueue(0), args)
  arp.startArpTask{
    { rxQueue = dev1:getRxQueue(2), txQueue = dev1:getTxQueue(2), ips = args.ipsrc }
  }
  mg.waitForTasks()
end

local function doArp(args)
  if not DST_MAC then
    log:info("Performing ARP lookup on %s", args.ipdst)
    DST_MAC = arp.blockingLookup(args.ipdst, 10)
    if not DST_MAC then
      log:info("ARP lookup failed, using default destination mac address")
      return
    end
  end
  log:info("Destination mac: %s", DST_MAC)
end

function loadSlave(queue, args)
  while mg.running() do
    doArp(args)
    mg.sleepMillis(1000)
  end
  print('DST_MAC', DST_MAC)
end

