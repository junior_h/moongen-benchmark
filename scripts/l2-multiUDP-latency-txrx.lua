local mg     = require "moongen"
local memory = require "memory"
local device = require "device"
local ts     = require "timestamping"
local filter = require "filter"
local hist   = require "histogram"
local stats  = require "stats"
local timer  = require "timer"
local arp    = require "proto.arp"
local log    = require "log"

local DEF_ETH_DST	= "0C:C4:7A:EA:06:B5"
local DEF_ETH_SRC	= "0C:C4:7A:EA:06:B4"
local DEF_IP_DST	= "10.0.33.11"
local DEF_IP_SRC	= "10.0.33.12"
local DEF_PORT_DST	= 319
local DEF_PORT_SRC	= 1234

function configure(parser)
  parser:description("Generates UDP traffic ")
  parser:argument("dev1", "Device 1 to transmit from."):convert(tonumber)
  parser:argument("dev2", "Device 2 to transmit from."):convert(tonumber)
  parser:option("-r --rate", "Transmit rate in Mbit/s."):default(10000):convert(tonumber)
  parser:option("-s --size", "Packet size."):default(100):convert(tonumber)
  parser:option("-S --macsrc", "Mac source."):default(DEF_ETH_SRC)
  parser:option("-D --macdst", "MAC destination."):default(DEF_ETH_DST)
  parser:option("--ipsrc", "MAC destination."):default(DEF_IP_SRC)
  parser:option("--ipdst", "MAC destination."):default(DEF_IP_DST)
  parser:option("--portsrc", "MAC destination."):default(DEF_PORT_SRC)
  parser:option("--portdst", "MAC destination."):default(DEF_PORT_DST)
  parser:option("-w --wait", "Max wait time of receive packet."):default(15):convert(tonumber)
end

function master(args)
    print('macsrc: ',args.macsrc)
    local dev1 = device.config({port = args.dev1, txQueues = 6, rxQueues = 3})
    local dev2 = device.config({port = args.dev2, txQueues = 6, rxQueues = 3})
    device.waitForLinks()
    if args.rate > 0 then
      dev1:getTxQueue(0):setRate(args.rate - (args.size + 4) * 8 / 1000)
    end
    mg.startTask("timerSlave", dev1:getTxQueue(0), dev2:getRxQueue(0), args)
    mg.waitForTasks()
end

local function fillUdpPacket(buf, args)
   buf:getUdpPacket():fill{
     ethSrc = args.macsrc,
     ethDst = args.macdst,
     ip4Src = args.ipsrc,
     ip4Dst = args.ipdst,
     udpSrc = args.portsrc,
     udpDst = args.portdst,
     pktLength = len
   }
end

function timerSlave(txQueue, rxQueue, args)
    --print('rxQueue.dev.useTimsyncIds: ' .. rxQueue.dev.useTimsyncIds)
    --print('getTimestamp: ' .. rxQueue:getTimestamp(nil, 1))
    --print('getTimestamp: ' .. txQueue:getTimestamp(nil, 1))
    local timestamper = ts:newUdpTimestamper(txQueue, rxQueue)
    local hist = hist:new()
    mg.sleepMillis(1000) -- ensure that the load task is running
    local counter = 0
    local rateLimit = timer:new(0.1)
    while mg.running() do
        local latency = timestamper:measureLatency(args.size, function(buf)
               fillUdpPacket(buf, args)
               --local pkt = buf:getUdpPacket()
               --pkt.ip4.src:set(baseIP + counter)
               --counter = incAndWrap(counter, flows)
               end,
               args.wait)
        tdump({latency})
        hist:update(latency)
        rateLimit:wait()
        rateLimit:reset()
    end
    mg.sleepMillis(500)
    hist:print()
    hist:save("histogram.csv")
end

function loadTxSlave(txq,args)
    local txbufs = memory.bufArray()
    local txQueue = txq
    local txCtr = stats:newDevTxCounter(txQueue, "plain")
    local mem = memory.createMemPool(
                    function(buf) 
                        fillUdpPacket(buf, args) 
                     end)
    local txbufs = mem:bufArray()
    mg.sleepMillis(2000)
    while mg.running() do
        txbufs:alloc(args.size)
        txbufs:offloadUdpChecksums()
        txQueue:send(txbufs)
        txCtr:update()
    end
    print("\n//=========TX COUNTER STATS========//")
    txCtr:finalize()
end

function loadRxSlave(rxq, args)
    local rxbufs = memory.bufArray()
    local rxQueue = rxq
    local rxCtr = stats:newDevRxCounter(rxQueue, "plain")
    local pktCtr = stats:newPktRxCounter("Packets counted", "plain")
    mg.sleepMillis(2000)
    while mg.running() do
        local rx = rxQueue:tryRecv(rxbufs,100)
        for i = 1, rx do
            local buf = rxbufs[i]
            if isDump then
                buf:dump()
            end
            pktCtr:countPacket(buf)
        end
        rxbufs:free(rx)
        pktCtr:update()  
    end
    print("//=========RX DEV STATS========//\n")
    printStats(rxQueue.dev:getStats())
    rxCtr:finalize()
    pktCtr:finalize()
end

function loadTxRxSlave(txq, rxq, args)
    local txbufs = memory.bufArray()
    local rxbufs = memory.bufArray()
    local rxQueue = rxq
    local txQueue = txq
    local rxCtr = stats:newDevRxCounter(rxQueue, "plain")
    local txCtr = stats:newDevTxCounter(txQueue, "plain")
    local pktCtr = stats:newPktRxCounter("Packets counted", "plain")

    local mem = memory.createMemPool(
                    function(buf) 
                        fillUdpPacket(buf, args) 
                     end)

    local txbufs = mem:bufArray()
    mg.sleepMillis(2000)
    while mg.running() do
        local rx = rxQueue:tryRecv(rxbufs,100)
        for i = 1, rx do
            local buf = rxbufs[i]
            if isDump then
                buf:dump()
            end
            pktCtr:countPacket(buf)
        end
        rxbufs:free(rx)
        pktCtr:update()

        txbufs:alloc(args.size)
        txbufs:offloadUdpChecksums()
        txQueue:send(txbufs)
        txCtr:update()
    end
    print("\n//=========TX COUNTER STATS========//")
    txCtr:finalize()
    print("//=========RX DEV STATS========//\n")
    printStats(rxQueue.dev:getStats())
    rxCtr:finalize()
    pktCtr:finalize()
end

function tdump ( tab, iter, indent )
   print('//=== TDUMP ===//')
   if iter == nil then iter = 10 end
   if indent == nil then indent = "" end
   indent = indent.."   "
   if type (tab) ~= "table" then
      print ("invoke with a table, you sent me a: ",type (tab) )
      return   
   end
   for k,v in pairs (tab) do
      if type (v) == "table" then
         print (indent.." "..k.." <TABLE> ",v)
         if iter > 1 then
            tdump (v, iter-1, indent)
         end
      elseif type (v) == "function" then
         print (indent.." "..k.." <FUNCTION> ",v) 
      elseif type (v) == "boolean" then
         print (indent.." "..k.." <BOOLEAN> ",v) 
      else
         print (indent.." "..k.." ".." = ".." "..v) 
      end
   end
end

function printStats (devStats)
    print("ipacktes: " .. tostring(devStats.ipackets))
    print("opacktes: " .. tostring(devStats.opackets))
    print("ibytes: " .. tostring(devStats.ibytes))
    print("obytes: " .. tostring(devStats.obytes))
    print("imissed: " .. tostring(devStats.imissed))
    print("ierrors: " .. tostring(devStats.ierrors))
    print("oerrors: " .. tostring(devStats.oerrors))
    print("rx_nombuf: " .. tostring(devStats.rx_nombuf))
    print("q_ipacktes[0]: " .. tostring(devStats.q_ipackets[0]))
    print("q_ipacktes[1]: " .. tostring(devStats.q_ipackets[1]))
    print("q_ipacktes[2]: " .. tostring(devStats.q_ipackets[2]))
    print("q_ipacktes[3]: " .. tostring(devStats.q_ipackets[3]))
end

