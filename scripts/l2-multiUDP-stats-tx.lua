local mg     = require "moongen"
local memory = require "memory"
local device = require "device"
local ts     = require "timestamping"
local filter = require "filter"
local hist   = require "histogram"
local stats  = require "stats"
local timer  = require "timer"
local arp    = require "proto.arp"
local log    = require "log"

local DEF_ETH_DST	= "0C:C4:7A:EA:06:B5"
local DEF_ETH_SRC	= "0C:C4:7A:EA:06:B4"

function configure(parser)
  parser:description("Generates UDP traffic ")
  parser:argument("dev1", "Device 1 to transmit from."):convert(tonumber)
  parser:argument("dev2", "Device 2 to transmit from."):convert(tonumber)
  parser:option("-r --rate", "Transmit rate in Mbit/s."):default(10000):convert(tonumber)
  parser:option("-s --size", "Packet size."):default(60):convert(tonumber)
  parser:option("-S --macsrc", "Packet size."):default(DEF_ETH_SRC)
  parser:option("-D --macdst", "Packet size."):default(DEF_ETH_DST)
end

function master(args)
    print('macsrc: ',args.macsrc)
    local dev1 = device.config({port = args.dev1, rxQueues = 3, txQueues = 3})
    local dev2 = device.config({port = args.dev2, rxQueues = 3, txQueues = 3})
    device.waitForLinks()
    if args.rate > 0 then
      dev1:getTxQueue(0):setRate(args.rate - (args.size + 4) * 8 / 1000)
    end
    mg.startTask("loadTxSlave", dev1:getTxQueue(0), args)
    mg.startTask("loadRxSlave", dev2:getRxQueue(0), args, 0)
    --mg.startSharedTask("loadRxSlave", dev2:getRxQueue(1), args, 1)
    --mg.startSharedTask("loadRxSlave", dev2:getRxQueue(2), args, 2)
    --mg.startSharedTask("loadRxSlave", dev2:getRxQueue(3), args, 3)
    mg.waitForTasks()
end

local function fillUdpPacket(buf, args)
   buf:getUdpPacket():fill{
     ethSrc = args.macsrc,
     ethDst = args.macdst,
     ethType = 0x1234
   }
end

function loadRxSlave(queue, args, id)
    local bufs = memory.bufArray()
    local rxQueue = queue
    local rxCtr = stats:newDevRxCounter(rxQueue.dev)
    local pktCtr = stats:newPktRxCounter("Packets received id-" .. id, "plain")
    while mg.running() do
        local rx = rxQueue:tryRecv(bufs,100)
        for i = 1, rx do
            local buf = bufs[i]
            if isDump then
                buf:dump()
            end
            pktCtr:countPacket(buf)
        end
        bufs:free(rx)
        pktCtr:update()
    end
    print("\n//=========RX DEV STATS========//")
    printStats(rxQueue.dev:getStats())
    print("//=========RX DEV STATS========//\n")
    pktCtr:finalize()
end

function loadTxSlave(queue, args)
    local mem = memory.createMemPool(
                    function(buf) 
                        fillUdpPacket(buf, args) 
                     end)
    local bufs = mem:bufArray()
    local txCtr = stats:newDevTxCounter(queue, "plain")
    while mg.running() do
        bufs:alloc(args.size)
        bufs:offloadUdpChecksums()
        queue:send(bufs)
        txCtr:update()
    end
    txCtr:finalize()
    print("\n//=========TX DEV STATS========//")
    printStats(queue.dev:getStats())
    print("//=========TX DEV STATS========//\n")
end

function tdump ( tab, iter, indent )
   print('//=== TDUMP ===//')
   if iter == nil then iter = 10 end
   if indent == nil then indent = "" end
   indent = indent.."   "
   if type (tab) ~= "table" then
      print ("invoke with a table, you sent me a: ",type (tab) )
      return   
   end
   for k,v in pairs (tab) do
      if type (v) == "table" then
         print (indent.." "..k.." <TABLE> ",v)
         if iter > 1 then
            tdump (v, iter-1, indent)
         end
      elseif type (v) == "function" then
         print (indent.." "..k.." <FUNCTION> ",v) 
      elseif type (v) == "boolean" then
         print (indent.." "..k.." <BOOLEAN> ",v) 
      else
         print (indent.." "..k.." ".." = ".." "..v) 
      end
   end
end

function printStats (devStats)
    print("ipacktes: " .. tostring(devStats.ipackets))
    print("opacktes: " .. tostring(devStats.opackets))
    print("ibytes: " .. tostring(devStats.ibytes))
    print("obytes: " .. tostring(devStats.obytes))
    print("imissed: " .. tostring(devStats.imissed))
    print("ierrors: " .. tostring(devStats.ierrors))
    print("oerrors: " .. tostring(devStats.oerrors))
    print("rx_nombuf: " .. tostring(devStats.rx_nombuf))
    print("q_ipacktes[0]: " .. tostring(devStats.q_ipackets[0]))
    print("q_ipacktes[1]: " .. tostring(devStats.q_ipackets[1]))
    print("q_ipacktes[2]: " .. tostring(devStats.q_ipackets[2]))
    print("q_ipacktes[3]: " .. tostring(devStats.q_ipackets[3]))
end

