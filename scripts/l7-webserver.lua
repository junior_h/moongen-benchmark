local mg     = require "moongen"
local memory = require "memory"
local device = require "device"
local ts     = require "timestamping"
local filter = require "filter"
local hist   = require "histogram"
local stats  = require "stats"
local timer  = require "timer"
local arp    = require "proto.arp"
local log    = require "log"
local server = require "webserver"

local DEF_ETH_DST       = "0C:C4:7A:EA:06:B5"
local DEF_ETH_SRC       = "0C:C4:7A:EA:06:B4"

function configure(parser)
	parser:argument("dev1", "Device to use, generates some dummy traffic to showcase the statistics API."):convert(tonumber)
	--parser:option("-r --rate", "Transmit rate in Mbit/s."):default(10000):convert(tonumber)
	parser:option("-p --port", "Start the REST API on the given port."):args(1):default(8080):convert(tonumber)
	parser:option("-b --bind", "Bind to a specific IP (existing IP interface).")
	parser:option("-S --macsrc", "MAC source."):default(DEF_ETH_SRC)
	parser:option("-D --macdst", "MAC destination."):default(DEF_ETH_DST)
end

function initWebserver(turbo, defaultResponse)
	log:info("Running webserver initializer, received argument: %s", defaultResponse)
	local helloWorld = class("helloWorld", turbo.web.RequestHandler)
	function helloWorld:get(pathArg)
		if pathArg == "" then
			pathArg = defaultResponse
		end
	    self:write({hello = pathArg})
	end
	function helloWorld:post(...)
		local json = self:get_json(true)
		if not json then
			error(turbo.web.HTTPError(400, {message = "Expected JSON data in POST body."}))
		end
		self:write(json)
	end
	return {
		{"^/hello/?([^/]*)$", helloWorld},
	}
end

function master(args)
    local dev1 = device.config{port = args.dev1, rxQueues = 3, txQueues = 3}
    device.waitForLinks()
    server.startWebserverTask({
        port = args.port,
        bind = args.bind,
        init = "initWebserver"
    }, "Hello, world")

    mg.startTask("loadSlave", dev1:getTxQueue(0), args)
    mg.waitForTasks()
end

function fillUdpPacket(buf, args)
   buf:getUdpPacket():fill{
     ethSrc = args.macsrc,
     ethDst = args.macdst,
     ethType = 0x1234
   }
end

function loadSlave(queue,args)
    local mempool = memory.createMemPool(
                    function(buf) 
                        fillUdpPacket(buf, args) 
                     end)
    local bufs = mempool:bufArray()
    print('send data')
    while mg.running() do
        bufs:alloc(60)
        bufs:offloadUdpChecksums()
        queue:send(bufs)
        mg.sleepMillis(1000)
    end
end

function tdump ( tab, iter, indent )
   print('//=== TDUMP ===//')
   if iter == nil then iter = 10 end
   if indent == nil then indent = "" end
   indent = indent.."   "
   if type (tab) ~= "table" then
      print ("invoke with a table, you sent me a: ",type (tab) )
      return   
   end
   for k,v in pairs (tab) do
      if type (v) == "table" then
         print (indent.." "..k.." <TABLE> ",v)
         if iter > 1 then
            tdump (v, iter-1, indent)
         end
      elseif type (v) == "function" then
         print (indent.." "..k.." <FUNCTION> ",v) 
      elseif type (v) == "boolean" then
         print (indent.." "..k.." <BOOLEAN> ",v) 
      else
         print (indent.." "..k.." ".." = ".." "..v) 
      end
   end
end

