-- e.g: MoonGen test-l3-tx-multiUDP.lua.latency.1 -r 100 -S 00:25:90:FA:AC:32 -D 94:3f:c2:c7:21:5e --ipsrc 10.0.13.44 --ipdst 10.0.12.21 --ipgw 10.0.13.9 0 0
--      MoonGen test-l3-tx-multiUDP.lua.latency.4 -r 7000 -S 00:25:90:FA:AC:33 -D 94:3f:c2:c7:21:5f --ipsrc 10.0.14.44 --ipdst 10.0.13.21 --ipgw 10.0.13.9 -w 100 1 0 -s 300 --load --stats --latency --latencydebug
--      MoonGen test-l3-tx-multiUDP.lua.latency.4 -c config.lua
-- =============

local mg     = require "moongen"
local memory = require "memory"
local device = require "device"
local ts     = require "timestamping"
local filter = require "filter"
local hist   = require "histogram"
local stats  = require "stats"
local timer  = require "timer"
local arp    = require "proto.arp"
local log    = require "log"

local DEF_ETH_DST	= "0C:C4:7A:EA:06:B5"
local DEF_ETH_SRC	= "0C:C4:7A:EA:06:B4"
local DEF_IP_DST	= "10.0.33.11"
local DEF_IP_SRC	= "10.0.34.12"
local DEF_IP_GW		= "10.0.33.9"  --next hop / gw IP
local DEF_PORT_DST	= 319
local DEF_PORT_SRC	= 1234

function configure(parser)
    parser:description("Generates UDP traffic ")
    parser:argument("dev1", "Device 1 to transmit from."):convert(tonumber)
    parser:argument("dev2", "Device 2 to transmit from."):convert(tonumber)
    parser:option("-c --config", "Config file.")
    parser:option("-r --rate", "Transmit rate in Mbit/s."):default(10000):convert(tonumber)
    parser:option("-s --size", "Packet size."):default(100):convert(tonumber)
    parser:option("-S --macsrc", "Mac source."):default(DEF_ETH_SRC)
    parser:option("-D --macdst", "MAC destination."):default(DEF_ETH_DST)
    parser:option("--ipsrc", "IP source."):default(DEF_IP_SRC)
    parser:option("--ipdst", "IP destination."):default(DEF_IP_DST)
    parser:option("--ipgw", "IP gateway / next hop."):default(DEF_IP_GW)
    parser:option("-f --flows", "Number of flows (randomized source IP)."):default(6):convert(tonumber)
    parser:option("--portsrc", "MAC destination."):default(DEF_PORT_SRC)
    parser:option("--portdst", "MAC destination."):default(DEF_PORT_DST)
    parser:option("-V --vlan", "VLAN Tagged of interface source."):default(DEF_PORT_DST)
    parser:option("-w --wait", "Max wait time of receive packet."):default(15):convert(tonumber)
    parser:option("--latencyrate", "Measure latency every X secondax."):default(1):convert(tonumber)
    parser:flag("--load", "generate packet.")
    parser:flag("--latency", "measure latency.")
    parser:flag("--latencydebug", "debug latency.")
    parser:flag("--stats", "debug queue stats.")
end

function master(args)
    print('Input args:')
    if args.config then
        print("load config: ", args.config)
        dofile(args.config)
        args = customArgs
    else
        args = {args}
    end
    for k,v in pairs(args) do
        print("PAIR", k)     
        args = v
        tdump(args,5)

        -- RUN MoonGen Threads
        local dev1 = device.config({port = args.dev1, txQueues = 6, rxQueues = 6})
        local dev2 = device.config({port = args.dev2, txQueues = 6, rxQueues = 6})
        device.waitForLinks()
        if args.rate > 0 then
            dev1:getTxQueue(0):setRate(args.rate - (args.size + 4) * 8 / 1000)
        end
        if args.load then
            mg.startTask("loadTxSlave", dev1:getTxQueue(0), args)
            mg.startTask("loadRxSlave", dev2:getRxQueue(0), args)
        end
        if args.latency then
            mg.startTask("timerSlave", dev1:getTxQueue(3), dev2:getRxQueue(3), args)
        end
        arp.startArpTask{
            { rxQueue = dev1:getRxQueue(2), txQueue = dev1:getTxQueue(2), ips = args.ipsrc },
            { rxQueue = dev2:getRxQueue(2), txQueue = dev2:getTxQueue(2), ips = args.ipdst }
        }
    end
    mg.waitForTasks()
end

local function fillUdpPacket(buf, args)
   buf:getUdpPacket():fill{
     ethSrc = args.macsrc,
     ethDst = args.macdst,
     ip4Src = args.ipsrc,
     ip4Dst = args.ipdst,
     udpSrc = args.portsrc,
     udpDst = args.portdst,
     pktLength = len
   }
end

function doArp(args)
    log:info("Performing ARP lookup on %s", args.ipgw)
    local DST_MAC1 = arp.blockingLookup(args.ipgw, 5)
    log:info("Destination mac ip gw / next hop %s: %s", args.ipgw, DST_MAC1)

    log:info("Performing ARP lookup on %s", args.ipdst)
    local DST_MAC2 = arp.blockingLookup(args.ipdst, 5)
    log:info("Destination mac ip destination %s: %s", args.ipdst, DST_MAC2)

    if not DST_MAC1 then
      log:info("ARP lookup failed, using default destination mac address 1")
    end
    if not DST_MAC2 then
      log:info("ARP lookup failed, using default destination mac address 2")
    end
end

function timerSlave(txQueue, rxQueue, args)
    doArp(args)
    local txCtr = stats:newDevTxCounter('Timer TX Device id=' .. txQueue.qid, txQueue, "plain")
    local rxCtr = stats:newDevRxCounter('Timer RX Device id=' .. rxQueue.qid, rxQueue, "plain")
    local timestamper = ts:newUdpTimestamper(txQueue, rxQueue)
    local hist = hist:new()
    mg.sleepMillis(1000) -- ensure that the load task is running
    local counter = 0
    local baseIP = parseIPAddress(args.ipsrc)
    local rateLimit = timer:new(args.latencyrate)
    print('====SEND DATA FOR MEASURING LATENCY====')
    local packetCount = 0
    while mg.running() do
        --packetCount = packetCount + 1
        --print("PACKET : ", packetCount)
        local latency = timestamper:measureLatency(args.size, function(buf)
               fillUdpPacket(buf, args)
               local pkt = buf:getUdpPacket()
               pkt.ip4.src:set(baseIP + counter)
               counter = incAndWrap(counter, 5)
               end,
               args.wait)
        if args.latencydebug then
            print('====DUMP LATENCY====')
            tdump({latency})
            txCtr:update()
            rxCtr:update()
        end
        hist:update(latency)
        rateLimit:wait()
        rateLimit:reset()
    end
    mg.sleepMillis(500)
    txCtr:update()
    rxCtr:update()

    print("//=========TX COUNT STATS========//\n")
    txCtr:finalize()

    print("//=========TX DEV STATS========//\n")
    print('getTxStats (pkts, bytes): ', txQueue.dev:getTxStats())
    printStats(txQueue.dev:getStats())

    print("//=========RX DEV STATS========//\n")
    print('getRxStats (pkts, bytes): ', txQueue.dev:getRxStats())
    printStats(rxQueue.dev:getStats())

    print("//=========HISTOGRAM STATS========//\n")
    hist:print()
    hist:save("histogram.csv")
end

function loadTxSlave(txq,args)
    doArp(args)
    print("Ready to send traffic")
    local txbufs = memory.bufArray()
    local txQueue = txq
    local txCtr = stats:newDevTxCounter("Stats TX Device id=" .. txQueue.qid, txQueue, "plain")
    local mem = memory.createMemPool(
                    function(buf) 
                        fillUdpPacket(buf, args) 
                     end)
    local txbufs = mem:bufArray()
    mg.sleepMillis(2000)
    while mg.running() do
        txbufs:alloc(args.size)
        txbufs:offloadUdpChecksums()
        txQueue:send(txbufs)
        if args.stats then
            txCtr:update()
        end
    end
    log:info("\n//=========TX COUNTER STATS - loadTxSlave========//")
    txCtr:update()
    txCtr:finalize()
    print('getTxStats (pkts, bytes): ', txQueue.dev:getTxStats())
    printStats(txQueue.dev:getStats())
    log:info("\n//=========TX COUNTER STATS - loadTxSlave========//")
end

function loadRxSlave(rxq, args)
    --doArp(args)
    print("Ready to receive traffic")
    local rxbufs = memory.bufArray()
    local rxQueue = rxq
    local rxCtr = stats:newDevRxCounter("Stats RX Device id=" .. rxQueue.qid, rxQueue, "plain")
    local pktCtr = stats:newPktRxCounter("Packets RX Device id-" .. rxQueue.qid, "plain")
    mg.sleepMillis(2000)
    while mg.running() do
        local rx = rxQueue:tryRecv(rxbufs,100)
        for i = 1, rx do
            local buf = rxbufs[i]
            if isDump then
                buf:dump()
            end
            pktCtr:countPacket(buf)
        end
        rxbufs:free(rx)
        if args.stats then
            pktCtr:update()
            rxCtr:update()
        end
    end
    pktCtr:update()
    log:info("\n//=========RX COUNTER STATS - loadRxSlave========//")
    rxCtr:finalize()
    pktCtr:finalize()
    print('getRxStats (pkts, bytes): ', rxQueue.dev:getRxStats())
    printStats(rxQueue.dev:getStats())
    log:info("\n//=========RX COUNTER STATS - loadRxSlave========//")
end

function loadTxRxSlave(txq, rxq, args)
    doArp(args)
    local txbufs = memory.bufArray()
    local rxbufs = memory.bufArray()
    local rxQueue = rxq
    local txQueue = txq
    local rxCtr = stats:newDevRxCounter(rxQueue, "plain")
    local txCtr = stats:newDevTxCounter(txQueue, "plain")
    local pktCtr = stats:newPktRxCounter("Packets counted", "plain")
    local mem = memory.createMemPool(
                    function(buf)
                        fillUdpPacket(buf, args)
                     end)
    local txbufs = mem:bufArray()
    mg.sleepMillis(2000)
    while mg.running() do
        local rx = rxQueue:tryRecv(rxbufs,100)
        for i = 1, rx do
            local buf = rxbufs[i]
            if isDump then
                buf:dump()
            end
            pktCtr:countPacket(buf)
        end
        rxbufs:free(rx)
        pktCtr:update()
        txbufs:alloc(args.size)
        txbufs:offloadUdpChecksums()
        txQueue:send(txbufs)
        txCtr:update()
    end

    print("\n//=========TX COUNTER STATS - loadTxRxSlave========//")
    txCtr:finalize()
    print('getTxStats (pkts, bytes): ', txQueue.dev:getTxStats())

    print("//=========RX DEV STATS - loadTxRxSlave========//\n")
    print('getRxStats (pkts, bytes): ', rxQueue.dev:getRxStats())
    printStats(rxQueue.dev:getStats())
    rxCtr:finalize()
    pktCtr:finalize()
end

function tdump ( tab, iter, indent )
   --print('//=== TDUMP ===//')
   if iter == nil then iter = 10 end
   if indent == nil then indent = "" end
   indent = indent.."   "
   if type (tab) ~= "table" then
      print ("invoke with a table, you sent me a: ",type (tab) )
      return   
   end
   for k,v in pairs (tab) do
      if type (v) == "table" then
         print (indent.." "..k.." <TABLE> ",v)
         if iter > 1 then
            tdump (v, iter-1, indent)
         end
      elseif type (v) == "function" then
         print (indent.." "..k.." <FUNCTION> ",v) 
      elseif type (v) == "boolean" then
         print (indent.." "..k.." <BOOLEAN> ",v) 
      else
         print (indent.." "..k.." ".." = ".." ",v) 
      end
   end
end

function printStats (devStats)
    print("ipacktes: " .. tostring(devStats.ipackets))
    print("opacktes: " .. tostring(devStats.opackets))
    print("ibytes: " .. tostring(devStats.ibytes))
    print("obytes: " .. tostring(devStats.obytes))
    print("imissed: " .. tostring(devStats.imissed))
    print("ierrors: " .. tostring(devStats.ierrors))
    print("oerrors: " .. tostring(devStats.oerrors))
    print("rx_nombuf: " .. tostring(devStats.rx_nombuf))
    print("q_ipacktes[0]: " .. tostring(devStats.q_ipackets[0]))
    print("q_ipacktes[1]: " .. tostring(devStats.q_ipackets[1]))
    print("q_ipacktes[2]: " .. tostring(devStats.q_ipackets[2]))
    print("q_ipacktes[3]: " .. tostring(devStats.q_ipackets[3]))
end

