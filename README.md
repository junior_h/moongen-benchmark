# README #

MoonGen is traffic generator based on libmoon & LUA. It has capability to generate pps at line rate, 12Mpps on 10Gbps NIC in single core CPU (1.5GHz)

### How to use? ###

* Install Dependency
* Install DPDK
* Install MoonGen

### Install DPDK (Debian) ###

* Install dependencies
apt-get install git build-essential linux-headers-`uname -r`

* Download source
wget http://fast.dpdk.org/rel/dpdk-17.11.tar.xz
tar -xvf dpdk-17.11.tar.xz
cd dpdk-17.11/usertools/
./dpdk-setup.sh
Choose: 
  [14] x86_64-native-linuxapp-gcc
  [17] Insert IGB UIO module
  [23] Bind Ethernet/Crypto device to IGB UIO module

Show status interface driver
./dpdk-devbind.py --status

Set interface to DPDK mode
./dpdk-devbind.py --status  -b igb_uio 0000:02:00.0

Unboind interface DPDK
./dpdk-devbind.py --status  -b ixgbe 0000:02:00.0
./dpdk-devbind.py --status  -b i40e 0000:02:00.0

### Install MoonGen ###

* Install dependencies
apt-get install git build-essential cmake linux-headers-`uname -r` pciutils libnuma-dev

* Compile
git clone https://github.com/emmericp/MoonGen.git
cd MoonGen
./build.sh

* Run lua script
build/MoonGen <lua_file> <args>




